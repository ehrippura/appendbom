//
//  AppDelegate.swift
//  AppendBOM
//
//  Created by Tzu Yi Lin on 2017/12/31.
//  Copyright © 2017年 Tzu-Yi Lin. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
        return sender == NSApplication.shared
    }

}


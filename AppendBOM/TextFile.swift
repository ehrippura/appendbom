//
//  TextFile.swift
//  AppendBOM
//
//  Created by Tzu Yi Lin on 2017/12/31.
//  Copyright © 2017年 Tzu-Yi Lin. All rights reserved.
//

import Foundation

private let bom: [UInt8] = [0xEF, 0xBB, 0xBF]

class TextFile {

    let fileURL: URL

    let containsBOM: Bool

    init(url: URL) {

        fileURL = url

        let input = InputStream(url: fileURL)!
        input.open()
        let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: bom.count)
        input.read(buffer, maxLength: bom.count)

        var bomCheck = true
        for (index, byte) in bom.enumerated() {
            if (buffer + index).pointee != byte {
                bomCheck = false
                break
            }
        }
        containsBOM = bomCheck

        input.close()
        buffer.deallocate()
    }

    func write() {

        guard let data = try? Data(contentsOf: fileURL), !containsBOM else {
            return
        }

        var newData = Data(bytes: bom, count: 3)
        newData.append(data)

        do {
            try newData.write(to: fileURL)
        } catch {
            print(error)
        }
    }
}

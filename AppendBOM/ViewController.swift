//
//  ViewController.swift
//  AppendBOM
//
//  Created by Tzu Yi Lin on 2017/12/31.
//  Copyright © 2017年 Tzu-Yi Lin. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    @IBOutlet weak var dragView: DragView!

    private var queue = DispatchQueue(label: "text process queue")

    override func viewDidLoad() {
        super.viewDidLoad()
        dragView.delegate = self
        dragView.accessibleTypes = [
            kUTTypeSourceCode,
            kUTTypeCSource,
            kUTTypeCHeader,
            kUTTypeCPlusPlusHeader,
            kUTTypeCPlusPlusSource,
            kUTTypeObjectiveCSource,
            kUTTypeObjectiveCPlusPlusSource,
            kUTTypeSwiftSource
        ].map { $0 as String }
    }
    
}

extension ViewController: DragViewHandler {

    func dragView(view: DragView, handleFileURLs urls: [URL]) {
        
        view.progressView.doubleValue = 0.0
        let count = urls.count

        queue.async {
            for (index, url) in urls.enumerated() {
                let targetUrl = (url.absoluteString.hasPrefix("file://")) ? url : (URL(fileURLWithPath: url.path))
                let textFile = TextFile(url: targetUrl)
                if !textFile.containsBOM {
                    textFile.write()
                }
                DispatchQueue.main.async {
                    view.progressView.doubleValue = Double(index + 1) / Double(count)
                }
            }
        }
    }
}


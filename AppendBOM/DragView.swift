//
//  DragView.swift
//  AppendBOM
//
//  Created by Tzu Yi Lin on 2017/12/31.
//  Copyright © 2017年 Tzu-Yi Lin. All rights reserved.
//

import Cocoa

protocol DragViewHandler: class {
    func dragView(view: DragView, handleFileURLs urls: [URL])
}

class DragView: NSView {

    @IBOutlet weak var imageView: NSImageView!
    @IBOutlet weak var progressView: NSProgressIndicator!
    
    var accessibleTypes: [String] = []

    var delegate: DragViewHandler?

    override func awakeFromNib() {
        super.awakeFromNib()
        registerForDraggedTypes([.fileURL])
        imageView.unregisterDraggedTypes()
    }

    override func draggingEntered(_ sender: NSDraggingInfo) -> NSDragOperation {

        let pBoard = sender.draggingPasteboard

        if let types = pBoard.types, types.contains(.fileURL) {
            return .generic
        }

        return []
    }

    override func performDragOperation(_ sender: NSDraggingInfo) -> Bool {

        guard let items = sender.draggingPasteboard.pasteboardItems else {
            return false
        }

        let ws = NSWorkspace.shared

        let urls = items.compactMap { item -> URL? in
            if let urlString = item.string(forType: .fileURL), let url = URL(string: urlString) {
                do {
                    let type = try ws.type(ofFile: url.path)
                    if accessibleTypes.contains(type) {
                        return url
                    } else {
                        return nil
                    }
                } catch {
                    print("error: \(error.localizedDescription)")
                    return nil
                }
            }
            return nil
        }

        if (urls.count != 0) {
            delegate?.dragView(view: self, handleFileURLs: urls)
            return true
        }

        return false
    }
}
